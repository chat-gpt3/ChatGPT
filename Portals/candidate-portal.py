import av
import openai
import streamlit as st
import speech_recognition as sr
from io import StringIO
from zipfile import ZipFile
from aiortc.contrib.media import MediaRecorder
from streamlit_webrtc import VideoProcessorBase, WebRtcMode, webrtc_streamer
m = st.markdown("""
<style>
div.stButton > button:first-child {
    background-color: #2ecc40;
    color: white;
    font-size: 8px;
    font-weight: 500;
}
</style>""", unsafe_allow_html=True)

r = sr.Recognizer()

if "page" not in st.session_state:
    st.session_state.page = 0

if "audio_recorded" not in st.session_state:
    st.session_state.audio_recorded = False


def nextpage(): 
    if sr.AudioFile("answer_audio.wav"):
        with sr.AudioFile("answer_audio.wav") as source:
            audio_data = r.record(source)
            text = r.recognize_google(audio_data)
            with open('Answers.txt', 'a') as file:
                for item in my_list[:5]:
                    if st.session_state.page == my_list.index(item) and st.session_state.page<5:
                        file.write("Question: " + item + "\n" )
                        file.write("Answer: " + text + "\n")
    st.session_state.page += 1

def submit(): 
    if sr.AudioFile("answer_audio.wav"):
        with sr.AudioFile("answer_audio.wav") as source:
            audio_data = r.record(source)
            text = r.recognize_google(audio_data)
            with open('Answers.txt', 'a') as file:
                for item in my_list[:5]:
                    if st.session_state.page == my_list.index(item) and st.session_state.page<5:
                        file.write("Question: " + item + "\n" )
                        file.write("Answer: " + text + "\n")
            if st.session_state.page == 1:
                st.success('Thankyou for answering the questions. \
                            The HR team will get back to you soon. Hoping to talk to you soon.')
    
class VideoProcessor(VideoProcessorBase):
    def recv(self, frame: av.VideoFrame) -> av.VideoFrame:
        img = frame.to_ndarray(format="bgr24")
        return av.VideoFrame.from_ndarray(img, format="bgr24")

def out_recorder_factory() -> MediaRecorder:
    return MediaRecorder( "answer_audio.wav", format="wav")

st.title('CANDIDATE PORTAL')
st.subheader('You will be asked 5 questions based on the information provided. \
    Please confirm that your microphone is connected. You need to answer each question in less than 3 mins.\
    Please speak clearly to the microphone. When you are ready click the Start button below.')

if "start_button_clicked" not in st.session_state:
    st.session_state.start_button_clicked = False

def callbackForStart():
    st.session_state.start_button_clicked = True 

if "audio_recorded" not in st.session_state:
    st.session_state.audio_recorded = False


def capture_start_stop(status):
    st.session_state.audio_recorded = status

model_engine = "text-davinci-003"
openai.api_key = "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" 

def ChatGPT(user_query):
    completion = openai.Completion.create(
                                  engine = model_engine,
                                  prompt = user_query,
                                  max_tokens = 1024,
                                  n = 1,
                                  temperature = 0.5)
    response = completion.choices[0].text
    return response

col1, col2, col3 = st.columns(3)  
if (col2.button('Start', on_click=callbackForStart) or st.session_state.start_button_clicked):
    st.subheader('Once you read the question click on Start and answer the question and then click Next.')
    placeholder = st.empty()
    
    with open('Questions.txt', 'r') as file:
        my_list = [line.strip() for line in file]
        for item in my_list:
            if st.session_state.page == my_list.index(item) and st.session_state.page<5:
                placeholder.text(item)
                ctx = webrtc_streamer(
                    key="loopback",
                    mode=WebRtcMode.SENDONLY,
                    rtc_configuration={"iceServers": [{"urls": ["stun:stun.l.google.com:19302"]}]},
                    media_stream_constraints={
                        "video": False,
                        "audio": True,
                    },
                    translations={
                        "start": "Start Recording",
                        "stop": "Stop Recording",
                        "select_device": "Choose Microphone device",
                    },
                    video_processor_factory=VideoProcessor,
                    in_recorder_factory=out_recorder_factory,
                    on_change=capture_start_stop(not st.session_state.audio_recorded)
                )
    col1, col2, col3 = st.columns(3) 
    if st.session_state.page < 1:
        col2.button("Next Question",on_click=nextpage)

    if st.session_state.page == 1:
        col1, col2, col3 = st.columns(3) 
        col2.button("Submit", on_click=submit)