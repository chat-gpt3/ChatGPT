
import av
import openai
import base64
import streamlit as st
import speech_recognition as sr
from io import StringIO
from zipfile import ZipFile
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from aiortc.contrib.media import MediaRecorder
from streamlit_webrtc import VideoProcessorBase
import os.path
import time, sched

m = st.markdown("""
<style>
div.stButton > button:first-child {
    background-color: #2ecc40;
    color: white;
    font-size: 8px;
    font-weight: 500;
}
</style>""", unsafe_allow_html=True)

r = sr.Recognizer()

model_engine = "text-davinci-003"
openai.api_key = "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" 

class VideoProcessor(VideoProcessorBase):
    def recv(self, frame: av.VideoFrame) -> av.VideoFrame:
        img = frame.to_ndarray(format="bgr24")
        return av.VideoFrame.from_ndarray(img, format="bgr24")

def out_recorder_factory() -> MediaRecorder:
    return MediaRecorder("user_recording.wav", format="wav")

def displayPDF(file):
  base64_pdf = base64.b64encode(file).decode('utf-8')
  pdf_display = F'<iframe src="data:application/pdf;base64,{base64_pdf}" width="700" height="1000" type="application/pdf"></iframe>'
  st.markdown(pdf_display, unsafe_allow_html=True)

def ChatGPT(user_query):
    completion = openai.Completion.create(
                                  engine = model_engine,
                                  prompt = user_query,
                                  max_tokens = 1024,
                                  n = 1,
                                  temperature = 0.5)
    response = completion.choices[0].text
    return response

def convert_pdf_to_txt_pages(path):
    texts = []
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, laparams=laparams)
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    size = 0
    c = 0
    file_pages = PDFPage.get_pages(path)
    nbPages = len(list(file_pages))
    for page in PDFPage.get_pages(path):
      interpreter.process_page(page)
      t = retstr.getvalue()
      if c == 0:
        texts.append(t)
      else:
        texts.append(t[size:])
      c = c+1
      size = len(t)
    device.close()
    retstr.close()
    return texts, nbPages

def convert_pdf_to_txt_file(path):
    texts = []
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, laparams=laparams)
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    file_pages = PDFPage.get_pages(path)
    nbPages = len(list(file_pages))
    for page in PDFPage.get_pages(path):
      interpreter.process_page(page)
      t = retstr.getvalue()
    device.close()
    retstr.close()
    return t, nbPages

def save_pages(pages):
  files = []
  for page in range(len(pages)):
    filename = "page_"+str(page)+".txt"
    with open("./file_pages/"+filename, 'w', encoding="utf-8") as file:
      file.write(pages[page])
      files.append(file.name)
  zipPath = './file_pages/pdf_to_txt.zip'
  zipObj = ZipFile(zipPath, 'w')
  for f in files:
    zipObj.write(f)
  zipObj.close()

  return zipPath

def evaluate():
    with open('Answers.txt', 'r') as file:
        user_query = "I interviewed a candidate. Can you help me evaluate his performance by grading(0-10) the answers he's given for the questions provided below with an explanation of each of the answer expected ? " + "\n" + file.read()
        # st.write("Interview Summary: " + user_query)
        response = ChatGPT(user_query)
        with open('EvaluationAutomated.txt', 'w') as file:
          file.write("%s\n" % response)


if os.path.isfile('./Answers.txt'):
  st.title('ADMIN PANEL')
  st.subheader('Candidate has sucessfully completed the evaluation.')
  st.caption('Click the link below to view the results')
  col1, col2, col3 = st.columns(3)  
  col2.button("Evaluate", on_click=evaluate)
else:
  st.title('HR PORTAL')
  name = st.text_input('Enter your name:')
  # years_exp = st.selectbox(
  #     'Enter years of experience:',
  #     ('', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'))
  years_exp = st.slider('Enter years of experience:', min_value=0, max_value=10, step=1, label_visibility="visible")
  # tech_stack = st.text_input('Enter Tech stack: ')
  tech_stack = st.selectbox(
      'Major Tech stack: ',
      ('', 'Angular', 'Java', 'React', 'PL/SQL', 'Android'))
  text_data_f = ''
  nbPages = ''

  pdf_file = st.file_uploader("Load your PDF", type="pdf")
  if pdf_file:
      path = pdf_file.read()
      # If needed to display the PDF
      # with st.expander("Display document"):
      #     displayPDF(path)
      text_data_f, nbPages = convert_pdf_to_txt_file(pdf_file)
      st.success('Resume uploaded sucessfully')
      # totalPages = "Pages: "+str(nbPages)+" in total"
      # st.info(totalPages)
      # If needed to Download the PDF
      # st.download_button("Download txt file", text_data_f)

  col1, col2, col3 = st.columns(3)  
  submitAction = col2.button('Submit', type="primary", disabled=not pdf_file, use_container_width=False)
  if submitAction:
    f = open("JD/JD_Angular.txt", "r")
    jobDescription = f.read()
       
    if tech_stack and years_exp and text_data_f:
      user_query = "I want to generate 10 technical questions for conducting an interview for a candidate having " + str(years_exp) \
                + " years of experience in " + tech_stack + ". The questions should involve programming concepts of the tech stack" \
                  " and questions specific to their resume. The questions should be of medium difficutly i.e on a scale of 1-10, it should be between 7-10."\
                  " The questions should not be of HR type. It should help me understand the knowledge of the candidate on the programming concepts."\
                  " I also want the questions in a list format to use in python. The job description of the role is as follows: \n" + jobDescription + \
                  "\nThe resume of the candidate is as follows: \n" + text_data_f
      response = ChatGPT(user_query)
      response = response[response.index('1.'):]
      my_list = response.split("\n")
      with open('Questions.txt', 'w') as file:
        for item in my_list:
            file.write("%s\n" % item)
      st.success('Thank you for entering the details. Candidate will now receive an email with the details.')
      # time.sleep(60)
      # st.experimental_rerun()